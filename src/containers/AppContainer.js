import React, { Component } from 'react';
import PropTypes from 'prop-types';
import '../styles.css';
import URLPicker from '../components/URLPicker';
import ReportedValues from '../components/ReportedValues';
import VideoPlayer from '../components/VideoPlayer';
import { parseMediaData } from '../util';

class StreamPlayerApp extends Component {
  constructor(props) {
    super(props);

    this.state = {
      loading: false,
      url: props.url,
      media: null,
      errorMessage: null
    };
  }

  loadMedia = (url) => {
    this.setState({
      url,
      loading: true
    });

    fetch(url)
      .then(response => response.text())
      .then(response => {
        const parser = new DOMParser();
        const xmlData = parser.parseFromString(response, "text/xml", 0);

        this.setState({
          loading: false,
          media: parseMediaData(xmlData),
          errorMessage: null
        });
      })
      .catch((err) => {
        this.setState({
          loading: false,
          errorMessage: "Error while fetching the url",
          media: null
        });
      });
  }

  renderPlayer() {
    const { loading, media } = this.state;

    if(loading) {
      return (
        <div>Loading...</div>
      );
    }

    if(media) {
      return (
        <div>
          <VideoPlayer 
            media={media}
            preloadDuration={20}
            onError={ (errorMessage) => { this.setState({ errorMessage }) } }
          />
          <ReportedValues videoData={media.video} />
        </div>
      );
    }

    return null;
  }

  render() {
    const { url, errorMessage } = this.state;

    return (
      <div className="StreamPlayerApp">
        { errorMessage && <div className="ErrorMessage">{ errorMessage }</div>}
        <URLPicker url={url} onPick={this.loadMedia}  />
        { this.renderPlayer() }
      </div>
    );
  }
}

StreamPlayerApp.propTypes = {
  url: PropTypes.string.isRequired
};

StreamPlayerApp.defaultProps = {
  url: 'http://rdmedia.bbc.co.uk/dash/ondemand/elephants_dream/1/client_manifest-one_representation_per_component.mpd'
};

export default StreamPlayerApp;
