import { getMediaSource, compareTimeRanges } from '../util';
import SegmentHandler from './SegmentHandler';
import VideoSource from './VideoSource';
import AudioSource from './AudioSource';

export default class MediaHandler {
  constructor(props) {
    const { media, preloadDuration, onUpdateVideoSource } = props;
    this.media = media;
    this.preloadDuration = preloadDuration;
    this.onUpdateVideoSource = onUpdateVideoSource;
    
    this.segmentHandler = new SegmentHandler(media);
    this._mediaSource = getMediaSource(media);
    this._videoUrl = URL.createObjectURL(this._mediaSource);
    this._lastSegmentLoaded = 1;
    this.isBuffering = false

    this.videoSource = null;
    this.audioSource = null;
    
    this._mediaSource.addEventListener('sourceopen', this.onSourceOpen, false);
  }

  get mediaSource() {
    return this._mediaSource;
  }

  set mediaSource(mediaSource) {
    this._mediaSource = mediaSource;
  }

  get videoUrl() {
    return this._videoUrl;
  }

  set videoUrl(videoUrl) {
    this._videoUrl = videoUrl;
  }

  get lastSegmentLoaded() {
    return this._lastSegmentLoaded;
  }

  set lastSegmentLoaded(_lastSegmentLoaded) {
    this._lastSegmentLoaded = _lastSegmentLoaded;
  }

  onSourceOpen = () => {
    const {
      audio: {
        audioCodecs,
        audioType
      },
      video: {
        videoCodecs,
        videoType,
        videoDurationInSec
      }
    } = this.media;

    try {
      this.videoSource = new VideoSource(this._mediaSource, videoType, videoCodecs, this.updateVideoSource);
      this.audioSource = new AudioSource(this._mediaSource, audioType, audioCodecs);
      
      this._mediaSource.duration = videoDurationInSec;

      this.segmentHandler.getInitialSegments().then(this.appendSegments);

    } catch (e) {
        console.error('Exception while creating source buffers: ', e);
    }

  }

  appendSegments = (buffers) => {
    const [videoBuffer, audioBuffer] = buffers;
    
    this.videoSource.append(videoBuffer);
    this.audioSource.append(audioBuffer);
  }

  updateVideoSource = () => {
      this.segmentHandler.getNextSegments(this.lastSegmentLoaded)
        .then(this.appendSegments)
        .then(() => {
          this.lastSegmentLoaded++;
          this.onUpdateVideoSource();
        });
  }

  bufferMedia(currentTime) {
    const {
      video: {
        segments
      }
    } = this.media;

    if (!this.isBuffering && this.lastSegmentLoaded < segments + 1) {
      if (compareTimeRanges(currentTime, this.videoSource.source.buffered) < this.preloadDuration) {
        this.isBuffering = true;

        this.segmentHandler.getNextSegments(this.lastSegmentLoaded)
          .then(this.appendSegments)
          .then(() => {
            this.lastSegmentLoaded++;
            this.isBuffering = false;
          });
      }
    } else {
      // this.playerRef.removeEventListener("timeupdate", this.handlePreload, false);
    }
  }

  onSeek = (currentTime) => {
    const {
      video: {
        segDuration
      }
    } = this.media;
    
    this.lastSegmentLoaded = Math.floor(currentTime / segDuration);
    return this.segmentHandler.getNextSegments(this.lastSegmentLoaded)
    .then(this.appendSegments)
  }

  onSeeked = () => {
    return this.segmentHandler.getNextSegments(this.lastSegmentLoaded)
      .then(this.appendSegments)
  }    
}