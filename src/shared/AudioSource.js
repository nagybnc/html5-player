export default class AudioSource {
    constructor(mediaSource, audioType, audioCodecs) {
  
      // if(!mediaSource) {
      //   throw new Error('Missing mediaSource');
      // }
  
      this.source = mediaSource.addSourceBuffer(audioType + '; codecs="' + audioCodecs + '"');
    }
    
    
    append(audioBuffer) {
      if(!this.source.updating) {
        this.source.appendBuffer(new Uint8Array(audioBuffer));
      }
    }
    
  }