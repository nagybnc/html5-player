import { getMediaName } from '../util';

export default class SegmentHandler {
  constructor(media) {

    if(!media) {
      throw new Error('Missing media data');
    }

    this.media = media;
  }

  getInitialSegments() {
    const {
      common: {
        initFileName
      }
    } = this.media;

    return this.fetchSegments(initFileName);
  }

  getNextSegments = (segmentIndex) => {
    const {
      common: {
        initMedinaName
      }
    } = this.media;
    
    const mediaName = getMediaName(initMedinaName, segmentIndex);
    
    return this.fetchSegments(`/${mediaName}`);
  }

  fetchSegments(segmentName) {
    const {
      audio: {
        audioAdaptationSetBaseURL,
        audioId
      },
      video: {
        videoAdaptationSetBaseURL,
        videoId
      },
      common: {
        baseURL
      }
    } = this.media;

    const videoUrl = `${baseURL}${videoAdaptationSetBaseURL}${videoId}${segmentName}`;
    const audioUrl = `${baseURL}${audioAdaptationSetBaseURL}${audioId}${segmentName}`;

    const urls = [
      fetch(videoUrl).then(response => response.arrayBuffer()),
      fetch(audioUrl).then(response => response.arrayBuffer()) 
    ];

    return Promise.all(urls);
  }

}