export default class VideoSource {
    constructor(mediaSource, videoType, videoCodecs, onUpdate) {
  
      // if(!mediaSource) {
      //   throw new Error('Missing mediaSource');
      // }

      this.mediaSource = mediaSource;
      this.source = mediaSource.addSourceBuffer(videoType + '; codecs="' + videoCodecs + '"');
      this.updateCallback = onUpdate;
  
      this.source.addEventListener("updateend", this.onUpdate, false);
    }
  
    onUpdate = () => {
      this.updateCallback(); 
      this.source.removeEventListener("updateend", this.onUpdate, false);
    }
    
    append(videoBuffer) {
      if(!this.source.updating) {
        this.source.appendBuffer(new Uint8Array(videoBuffer));
      }
    }
  
  }