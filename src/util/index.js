export function parseMediaData(data) {
    // COMMON VARIABLES
    const common = {
        'mediaPresentationDuration': data.querySelectorAll("MPD")[0].getAttribute("mediaPresentationDuration"),
        'baseURL': data.querySelectorAll("BaseURL")[0].textContent.toString(),
        'adaptationSets': data.querySelectorAll("AdaptationSet"),
        'rep': data.querySelectorAll("Representation"),
        'segmentTemplates': data.querySelectorAll("SegmentTemplate"),
        'initFileName': "/IS.mp4",
        'initMedinaName': "$Number%06d$.m4s"
    };
    // END COMMON VARIABLES

    // VIDEO VARIABLES
    const video = {
        'width': common.rep[0].getAttribute("width"),
        'height': common.rep[0].getAttribute("height"),
        'videoAdaptationSetBaseURL': common.adaptationSets[0].querySelectorAll("BaseURL")[0].textContent.toString(),
        'videoType': common.adaptationSets[0].getAttribute("mimeType"),
        'videoId': common.rep[0].getAttribute("id"),
        'videoCodecs': common.rep[0].getAttribute("codecs"),
        'videoBandwidth': common.rep[0].getAttribute("bandwidth"),
        'videoDuration': parseDuration(common.mediaPresentationDuration),
        'videoDurationInSec': parseDurationToSec(common.mediaPresentationDuration),
    };
    video.segDuration = +common.segmentTemplates[0].getAttribute("duration") / 1000;
    video.segments = Math.ceil(video.videoDurationInSec / video.segDuration);
    // END VIDEO VARIABLES

    // AUDIO VARIABLES
    const audio = {
        'audioAdaptationSetBaseURL': common.adaptationSets[1].querySelectorAll("BaseURL")[0].textContent.toString(),
        'audioType': common.adaptationSets[1].getAttribute("mimeType"),
        'audioId': common.rep[1].getAttribute("id"),
        'audioCodecs': common.adaptationSets[1].getAttribute("codecs")
    }; 
    // END AUDIO VARIABLES
    
    return {
        common,
        video,
        audio
    };
}

// converts mpd time to human time
const parseDuration = (pt) => {
    // parse time from format "PT#H#M##.##S"
    return pt
        .slice(2)
        .replace(/(H|M|S)/g, "$1 ")
        .toLowerCase()
        .trim();
}

const parseDurationToSec = (pt) => {
    // parse time to sec from format "PT#H#M##.##S"
    return pt
        .slice(2)
        .replace(/H|M|S/gi, '-')
        .split('-')
        .slice(0, -1)
        .reverse()
        .reduce((currentValue, item, index) => {
            return currentValue + item * Math.pow(60, index)
        }, 0)
}

//  Converts time in seconds into a string HH:MM:SS.ss
export function formatTime(timeSec) {
    let seconds = timeSec % 60;                                  //  get seconds portion                   
    let minutes = ((timeSec - seconds) / 60) % 60;              //  get minutes portion
    let hours = ((timeSec - seconds - (minutes * 60))) / 3600;  //  get hours portion
    seconds = seconds.toFixed(2);   // Restrict to 2 places (hundredths of seconds)
    let dispSeconds = seconds.toString().split(".");

    return (
        hours.toString().padStart(2, '0') + ":" +
        minutes.toString().padStart(2, '0') + ":" +
        dispSeconds[0].toString().padStart(2, '0') + "." +
        dispSeconds[1].toString().padStart(2, '0')
    );
}

export function getMediaSource({video, audio}) {
    if (
        'MediaSource' in window &&
        MediaSource.isTypeSupported(video.videoType +'; codecs="' + video.videoCodecs + '"') &&
        MediaSource.isTypeSupported(audio.audioType +'; codecs="' + audio.audioCodecs + '"')
    ) {
        return new window.MediaSource;
    } else {
        return null;
    }
}

export function compareTimeRanges(currentTime, videoSource) {
    if (videoSource.length >= 1 && currentTime > 0) {
        return videoSource.end(videoSource.length - 1) - currentTime;
    }
}

export function getMediaName(initMedinaName, segmentIndex) {
    return initMedinaName.replace("$Number%06d$", segmentIndex.toString().padStart(6, "0"));
}