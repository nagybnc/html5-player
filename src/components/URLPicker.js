import React, { Component } from 'react';
import PropTypes from 'prop-types';

class URLPicker extends Component {
  constructor(props) {
    super(props);

    this.state = {
      value: props.url || ''
    };

  }
    
  handleChange = (event) => {
    this.setState({
      value: event.target.value
    });
  }

  handleLoad = (event) => {
    event.preventDefault();

    this.props.onPick(this.state.value)
  }

  render() {
    return (
      <div className="URLPicker">
        <form onSubmit={this.handleLoad}>
          <label>
            Video Url:
            <input className="Input" type="text" value={this.state.value} onChange={this.handleChange} />
          </label>
          <input className="Button" type="submit" value="Load" />
        </form>
      </div>
    );
  }
  
}

URLPicker.propTypes = {
  url: PropTypes.string,
  onPick: PropTypes.func
}

export default URLPicker;
