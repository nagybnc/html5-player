import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { formatTime } from '../util';
import MediaHandler from '../shared/MediaHandler';

class VideoPlayer extends Component {
  constructor(props) {
    super(props);

    this.state = {
      segmentIndex: 1,
      elapsedTime: null
    }

    if(props.media) {
      this.playerRef = null;
      this.mediaHandler = new MediaHandler({
        media: props.media,
        preloadDuration: props.preloadDuration,
        onUpdateVideoSource: this.onUpdateVideoSource
      });
      this.videoUrl = this.mediaHandler.videoUrl;
    }
  }

  onUpdateVideoSource = () => {
    this.playerRef.addEventListener("timeupdate", this.onTimeUpdate, false);
    this.playerRef.addEventListener("seeking", this.onPlayerSeek, false);
    this.playerRef.addEventListener("seeked", this.onPlayerSeeked, false);
  }

  onTimeUpdate = () => {
    const { currentTime } = this.playerRef;
    
    this.mediaHandler.bufferMedia(currentTime);
    
    this.setState({
      elapsedTime: Math.round(currentTime),
      segmentIndex: this.mediaHandler.lastSegmentLoaded
    });

  }

  onPlayerSeek = () => {
    const { currentTime } = this.playerRef;

    this.playerRef.pause();
    this.mediaHandler.onSeek(currentTime).then(() => {
      this.playerRef.play();
    });

    this.setState({
      segmentIndex: this.mediaHandler.lastSegmentLoaded
    });
  }

  onPlayerSeeked = () => {
    this.mediaHandler.onSeeked().then(() => {
      this.playerRef.play();
    });
  }

  render() {
    return (
      <div className="Video">
        <div className="VideoInfo">
          <div className="InfoItem">
            <p className="InfoKey">Playing index</p>
            <p className="InfoValue">{ Math.floor(this.state.elapsedTime / this.props.media.video.segDuration) }</p>
          </div>

          <div className="InfoItem">
            <p className="InfoKey">Downloaded index</p>
            <p className="InfoValue">{ this.state.segmentIndex }</p>
          </div>

          <div className="InfoItem">
            <p className="InfoKey">Elapsed time</p>
            <p className="InfoValue">{ formatTime(this.state.elapsedTime) }</p>
          </div>
        </div>

        <video 
            ref={(el) => { this.playerRef = el; }}
            id="myVideo" 
            controls
            src={ this.videoUrl }
        >
          No video available
        </video>
      </div>
    );
  }
}

VideoPlayer.propTypes = {
  media: PropTypes.object.isRequired,
  preloadDuration: PropTypes.number,
  onError: PropTypes.func
};

VideoPlayer.defaultProps = {
  preloadDuration: 20,
  onError: () => {}
};

export default VideoPlayer;