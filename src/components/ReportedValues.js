import React from 'react';

export default ({ videoData }) => {
  const {
    videoType,
    videoCodecs,
    width,
    height,
    videoBandwidth,
    segDuration,
    videoDuration
  } = videoData;

  return (
    <div className="ReportedValues">
      <h3>Reported video values:</h3>
      <ul>
        <li>Type: {videoType} </li>
        <li>Codecs: {videoCodecs}</li>
        <li>Width: {width} -- Height: {height}</li>
        <li>Bandwidth: {videoBandwidth}</li>
        <li>Segment length: {segDuration} seconds</li>
        <li>Video duration: {videoDuration}</li>
      </ul>
    </div>
  );
};
